### Project structure :

## FACULTY VIEW
    -faculty-view 
        --assets
            --styles
                ---user
                ---console
            --global.module.scss
        --commons
            --lib
            --store
                ---user
                --console
        --components
            --globals
            --user
            --console
        --pages
            --api
            --console
            --user
            --app.js
            --404.js
            --index.js
        --public
            --favicon.ico
            --vercel.svg
        -.gitignore
        -package-lock.json
        -package.json
        -postcss.config.js
        -tailwind.config.js
    -README.md
    -STRUCTURE.md


### DESCRIPTION

1. faculty-view : contains app for faculty User, their dashboards and all relative files.

2. assets : contains style assets for user and console and a global.scss file for implementing said styles

3. commons : contains shared data store, helper functions to be used app-wide. 

4. components : contains components for globally shared items, user and console views.

5. pages : contains pages for /user and /console URL's. Also contains 'api routes' 

6. public : contains public assets such as favicon and logo

7. README.md : contains project definition, commonly described functions and use cases.

8. STRUCTURE.md : contains detailed project structure
