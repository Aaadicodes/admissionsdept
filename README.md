# ADMISSIONS DEPARTMENT MODULE

This contains the app for admissions dept with 3 views with their user(s) : 
1. faculty-view app : Faculty User
2. Admin User
3. Console View


### Satisfies the following user stories : 
1. 


Commonly used terms : 
1. projects : Individual projects assigned as 'program-stream-year-academic_year' eg Btech-CS-SY-2021-22


## Expected features to be implemented : 
#### Faculty view
1. Complete Overview of a particular project by faculty
2. Faculty verifies uploaded documents student-wise form-wise within a project
3. Verify applications for having all correct documents within a project
4. Generate a merit list out of verified applications based on fixed-rules and custom filters
5. Faculty promotes applicant to 'student' after verified documents, name in merit list , and fees paid.
6. User based project-wise view


## Technologies used :
1. Frontend : React, Next.js , typescript, Tailwind.css

## Packages implemented :
1. Tailwind.css

For detailed information about overall project structure, see STRUCTURE.md 
